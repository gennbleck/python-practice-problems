# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#
values = [4,5,6,4]
def max_in_list(values):
    return max(values)
print(max_in_list(values))
