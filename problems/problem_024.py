# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(lst):

    if len(lst) == 0:
        return
    else:
        return sum(lst) / len(lst)
lst = []

average = calculate_average(lst)

print (average), round(average, 9)
